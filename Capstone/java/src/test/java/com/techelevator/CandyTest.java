package com.techelevator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test; 


public class CandyTest {
	
	Candy snickers;
	
	@Before
	public void setup() {
		snickers = new Candy("Candy");
		
	}

	
	@Test
	public void candy_returns_correct_sound() {
		Assert.assertEquals("Munch Munch, Yum!", snickers.getSound());
	}
}
