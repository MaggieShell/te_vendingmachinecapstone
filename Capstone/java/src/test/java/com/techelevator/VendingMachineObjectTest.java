package com.techelevator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.techelevator.Chip;
import com.techelevator.Items;
import com.techelevator.VendingMachineObject; 



public class VendingMachineObjectTest {
	VendingMachineObject vm;
	
	@Before
	public void setup() {
		vm = new VendingMachineObject("C:\\Users\\Student\\workspace\\java-week-4-pair-6\\Capstone\\Example Files\\VMObjectSetupTest.txt");
		vm.setup();
	}
	
	@Test
	public void vending_machine_display_items_returns_as_expected() {
		ArrayList<String> testerList = new ArrayList<String>();
		Items testItem = new Chip("Chip");
		testItem.setSlot("A1");
		testItem.setName("Potato Crisps");
		testItem.setPrice(3.05);
		Items test2Item = new Chip("Chip");
		test2Item.setSlot("A2");
		test2Item.setName("Stackers");
		test2Item.setPrice(1.45);
		testerList.add(testItem.toString());
		testerList.add(test2Item.toString());
		ArrayList<String> vmList = vm.displayItems();
		Assert.assertEquals(testerList, vmList);
	}
	
	

}
