package com.techelevator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test; 

public class DrinkTest {
	Drink pepsi;
	
	@Before
	public void setup() {
		pepsi = new Drink("Drink");
		
	}

	
	@Test
	public void drink_returns_correct_sound() {
		Assert.assertEquals("Glug Glug, Yum!", pepsi.getSound());
	}

}
