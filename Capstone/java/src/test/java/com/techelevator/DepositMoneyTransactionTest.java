package com.techelevator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DepositMoneyTransactionTest {
	
DepositMoneyTransaction sale;
	
	@Before
	public void setup() {
		
		LocalDateTime date = LocalDateTime.of(2040, 11, 9, 1, 20, 15);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss");
		String formattedDate = date.format(formatter);
		sale = new DepositMoneyTransaction(formattedDate, "FEED MONEY", 2.50, 20.00);
	}
	
	@Test
	public void to_string_returns_correct_string(){
		Assert.assertEquals("11-09-2040 01:20:15 FEED MONEY $2.50 $20.00", sale.toString());
	}
}