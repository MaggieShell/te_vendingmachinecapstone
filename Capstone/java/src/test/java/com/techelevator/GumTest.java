package com.techelevator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test; 

public class GumTest {
	
Gum bubbleTape;
	
	@Before
	public void setup() {
		bubbleTape = new Gum ("Gum");
		
	}

	
	@Test
	public void gum_returns_correct_sound() {
		Assert.assertEquals("Chew Chew, Yum!", bubbleTape.getSound());
	}
	

}
