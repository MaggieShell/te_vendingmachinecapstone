package com.techelevator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test; 

public class BuyProductTransactionTest {
	
	BuyProductTransaction sale;
	
	@Before
	public void setup() {
		
		LocalDateTime date = LocalDateTime.of(2002, 04, 06, 12, 04, 45);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss");
		String formattedDate = date.format(formatter);
		sale = new BuyProductTransaction(formattedDate, "Mountain Melter", 1.50, 5.00);
		
		sale.setItemSlot("Z3"); //Accounts for setItemSlot test
	}
	
	@Test
	public void get_item_slot_returns_correctly () {
		Assert.assertEquals("Z3", sale.getItemSlot());
	}
	
	@Test
	public void to_string_returns_correct_string(){
		Assert.assertEquals("04-06-2002 12:04:45 Mountain Melter Z3 $1.50 $5.00", sale.toString());
	}
	
	

}
