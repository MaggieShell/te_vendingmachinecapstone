package com.techelevator;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test; 

public class ChipTest {
	
		Chip fritos;
		
		@Before
		public void setup() {
			fritos = new Chip("Chip");
			
		}

		
		@Test
		public void chip_returns_correct_sound() {
			Assert.assertEquals("Crunch Crunch, Yum!", fritos.getSound());
		}
}


