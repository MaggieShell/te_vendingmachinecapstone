package com.techelevator;

public class DepositMoneyTransaction extends Transaction {

	public DepositMoneyTransaction(String dateTime, String itemName, double itemPrice,
			double remainingBalance) {
		super(dateTime, itemName, itemPrice, remainingBalance);
		
	}

	
	@Override
	public String toString() {
		return String.format("%s %s $%.02f $%.02f", dateTime, itemName, itemPrice, remainingBalance);
	}
}
