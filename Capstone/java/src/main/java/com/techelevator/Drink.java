package com.techelevator;

public class Drink extends Items {
	

	public Drink(String type) {
		super(type);
	}

	@Override
		public String getSound() {
		
		return "Glug Glug, Yum!";
	}

	}
