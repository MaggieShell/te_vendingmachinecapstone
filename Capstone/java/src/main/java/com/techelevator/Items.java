package com.techelevator;

public abstract class Items implements Printable{
	
	private String name;
	private double price;
	private String type;
	private String slot;
	private int amountAvailable;
	private String sound;
	private int amountSold;
	
	

	public Items(String type) {
		this.type = type;
		this.setAmountAvailable(5);
		this.setAmountSold(0);
	}
	
	public int getAmountSold() {
		return amountSold;
	}

	public void setAmountSold(int amountSold) {
		this.amountSold = amountSold;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrice() {
		return price;
	}
	public String getSlot() {
		return slot;
	}

	public void setSlot(String slot) {
		this.slot = slot;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public int getAmountAvailable() {
		return amountAvailable;
	}

	public void setAmountAvailable(int amountAvailable) {
		this.amountAvailable = amountAvailable;
	}
	
	@Override
	public String getSound() {
		return sound;
	}
	@Override
	public String toString() {
		String result;
		if(this.getAmountAvailable() == 0) {
			result = "SOLD OUT!!";
		}else {
			result = String.format("%-5s %20s $%5.02f", this.getSlot(), this.getName(), this.getPrice());
		}return result;
	}

}
