package com.techelevator;

public class Candy extends Items {
	
	public Candy(String type) {
		super(type);
	}

	@Override
		public String getSound() {
		
		return "Munch Munch, Yum!";
	}

}
