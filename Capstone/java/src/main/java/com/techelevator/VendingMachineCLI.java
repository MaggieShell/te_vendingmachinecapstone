package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.techelevator.view.Menu;



public class VendingMachineCLI {
	
	private static Scanner userInput = new Scanner(System.in);

	private static final String MAIN_MENU_OPTION_DISPLAY_ITEMS = "Display Vending Machine Items";
	private static final String MAIN_MENU_OPTION_PURCHASE = "Purchase";
	private static final String MAIN_MENU_OPTION_EXIT = "Exit";
	private static final String MAIN_MENU_OPTION_SALES_REPORT = "Sales Report";
	private static final String[] MAIN_MENU_OPTIONS = { MAIN_MENU_OPTION_DISPLAY_ITEMS, MAIN_MENU_OPTION_PURCHASE, MAIN_MENU_OPTION_EXIT, MAIN_MENU_OPTION_SALES_REPORT };
	private static final String PURCHASE_MENU_OPTION_FEED_MONEY = "Feed Money";
	private static final String PURCHASE_MENU_OPTION_SELECT_PRODUCT = "Select Product";
	private static final String PURCHASE_MENU_OPTION_FINISH_TRANSACTION = "Finish Transaction";
	private static final String[] PURCHASE_MENU_OPTIONS = {PURCHASE_MENU_OPTION_FEED_MONEY, PURCHASE_MENU_OPTION_SELECT_PRODUCT, PURCHASE_MENU_OPTION_FINISH_TRANSACTION };

	
	private Menu menu;

	
	public VendingMachineCLI(Menu menu) {
		this.menu = menu;
	}

	public void run() {
		VendingMachineObject test = new VendingMachineObject("C:\\Users\\Student\\workspace\\java-week-4-pair-6\\Capstone\\Example Files\\VendingMachine.txt");
		test.setup();
		SalesReportWriter srw = new SalesReportWriter();
		
		boolean programRunning = true;
		while (programRunning) {
			String choice = (String) menu.getChoiceFromOptions(MAIN_MENU_OPTIONS, 3);

			if (choice.equals(MAIN_MENU_OPTION_DISPLAY_ITEMS)) {
				for(String str : test.displayItems()) {
					System.out.println(str);
				}
				
			} else if (choice.equals(MAIN_MENU_OPTION_PURCHASE)) {
				boolean inPurchaseMenu = true;
				while (inPurchaseMenu) {
					String choice2 = (String) menu.getChoiceFromOptions(PURCHASE_MENU_OPTIONS);

					if (choice2.equals(PURCHASE_MENU_OPTION_FEED_MONEY)) {
						System.out.print("Deposit amount in dollars: ");
						try {
							double deposit = Double.parseDouble(userInput.nextLine());
							if(deposit % 1 == 0) {
								test.feedMoney(deposit);
							}else {
								throw new Exception();
							}
						}catch (Exception e) {
							System.out.println("Please only enter whole numbers...");
						}
						
						System.out.format("Balance: $%.02f", test.getBalance());
					
					} else if (choice2.equals(PURCHASE_MENU_OPTION_SELECT_PRODUCT)) { 
						for(String str : test.displayItems()) {
							System.out.println(str);
						}
						System.out.println("Select Slot: ");
						String selection = userInput.nextLine();
						System.out.println(test.selectProduct(selection));
						System.out.format("%s %.02f", "Your current balance is: $",test.getBalance());
					}else if(choice2.equals(PURCHASE_MENU_OPTION_SELECT_PRODUCT)) {
							System.out.println("You're penniless, and must deposit money.");
					} else if (choice2.equals(PURCHASE_MENU_OPTION_FINISH_TRANSACTION)) {
						System.out.println(test.finishTransaction());
						inPurchaseMenu = false;
					}
				}
			}else if(choice.equals(MAIN_MENU_OPTION_EXIT)) {
				programRunning = false;
			}else if(choice.equals(MAIN_MENU_OPTION_SALES_REPORT)) {
				srw.writer(test);
				System.out.println("Sales Report has been generated!");
			}
		}
	}

	public static void main(String[] args) {
		Menu menu = new Menu(System.in, System.out);
		VendingMachineCLI cli = new VendingMachineCLI(menu);
		cli.run();
	}
}
