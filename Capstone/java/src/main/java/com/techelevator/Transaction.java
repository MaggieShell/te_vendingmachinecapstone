package com.techelevator;

public abstract class Transaction {
	
	String dateTime;
	String itemName;
	String itemSlot;
	double itemPrice;
	double remainingBalance;
	
	public Transaction(String dateTime, String itemName, double itemPrice, double remainingBalance) {
		this.dateTime = dateTime;
		this.itemName = itemName;
		this.itemPrice = itemPrice;
		this.remainingBalance = remainingBalance;
	}
	
}
