package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class SalesReportWriter {

	File file3 = new File("C:\\Users\\Student\\workspace\\java-week-4-pair-6\\Capstone\\Example Files\\TestSalesReport.txt");
	
	public void writer(VendingMachineObject vend) {
		try(PrintWriter writer = new PrintWriter (new FileOutputStream(file3, false))){
			for(Items itm : vend.getInventory()) {
				writer.append(itm.getName() + "|" + itm.getAmountSold() + "\n");
			}
			writer.append(String.format("\n %s $%.02f\n","**TOTAL SALES**", vend.getProfit()));
			
		}catch(FileNotFoundException e) {
			System.out.println("Could not write Sales Report!");
		}

	}
}
