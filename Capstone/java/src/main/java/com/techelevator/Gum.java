package com.techelevator;

public class Gum extends Items {

	public Gum(String type) {
		super(type);
	}

	@Override
		public String getSound() {
		
		return "Chew Chew, Yum!";
	}

	}
