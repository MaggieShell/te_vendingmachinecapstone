package com.techelevator;

public class BuyProductTransaction extends Transaction {

	private String itemSlot;
	
	public BuyProductTransaction(String dateTime, String itemName, double itemPrice, double remainingBalance) {
		super(dateTime, itemName, itemPrice, remainingBalance);
	}
	
	public String getItemSlot() {
		return itemSlot;
	}

	public void setItemSlot(String itemSlot) {
		this.itemSlot = itemSlot;
	}

	@Override
	public String toString() {
		return String.format("%s %s %s $%.02f $%.02f", dateTime, itemName, itemSlot, itemPrice, remainingBalance);

	}

}
