package com.techelevator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.techelevator.Candy;
import com.techelevator.Chip;
import com.techelevator.Drink;
import com.techelevator.Gum;
import com.techelevator.Items;

public class VendingMachineObject {
	
	ArrayList<Items> inventory = new ArrayList<Items>();
	ArrayList<String> salesReport = new ArrayList<String>();
	Map<String, Items> inventoryMap = new HashMap<String, Items>();
	File file;
	
	AuditWriter auditReport = new AuditWriter();
	
	private double balance = 0;
	private double profit = 0;
	
	public VendingMachineObject(String fileName) {
		file = new File(fileName);
	}

	public void setup() {
		try(Scanner fileInput = new Scanner(file)){
			while(fileInput.hasNextLine()) {
				String currentLine = fileInput.nextLine();
				String [] strArray = currentLine.split("\\|");
				Items itm;
				if(strArray[3].equals("Chip")) {
					itm = new Chip(strArray[3]);
				}else if(strArray[3].equals("Candy")) {
					itm = new Candy(strArray[3]);
				}else if(strArray[3].equals("Drink")) {
					itm = new Drink(strArray[3]);
				}else{
					itm = new Gum(strArray[3]);
				}
			
				itm.setSlot(strArray[0]);
				itm.setName(strArray[1]);
				itm.setPrice(Double.parseDouble(strArray[2]));
				inventory.add(itm);
				inventoryMap.put(itm.getSlot(), itm);
			}
		}catch (FileNotFoundException e) {
			System.out.println("File not found!");
		}
	}

	public double getBalance() {
		return balance;
	}
	
	public double getProfit() {
		return profit;
	}
	
	public ArrayList<String> displayItems() {
		ArrayList<String> setup = new ArrayList<String>();
		for(Items itm : inventory) {
			setup.add(itm.toString());
		}return setup;
	}
	
	public void feedMoney(double deposit) {
		if(deposit > 0) {
			balance += deposit;
			LocalDateTime date = LocalDateTime.now();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss");
			String formattedDate = date.format(formatter);
			DepositMoneyTransaction dep = new DepositMoneyTransaction(formattedDate + "", "FEED MONEY", deposit, balance);
			auditReport.writer(dep);
		}
	}
	
	public boolean hasEnoughMoney(String slotName) {
		boolean result = false;
		if(balance > inventoryMap.get(slotName).getPrice()) {
			result = true;
		}return result;
	}


	public String selectProduct(String selection) {
		String result;
		if(inventoryMap.containsKey(selection) && inventoryMap.get(selection).getAmountAvailable() > 0) {
			if(!hasEnoughMoney(selection)) {
				result = "Not enough money!";
			}
			else {
				result = buyProduct(selection);
			}
		}else {
			result = "Invalid slot!";
		}return result;
	}
	
	public String finishTransaction() {
		int nickles = 0;
		int dimes = 0;
		int quarters = 0;
		int tempProfit = (int)(balance * 100);
		while(tempProfit >= 25) {
			tempProfit -= 25;
			quarters++;
		}
		while(tempProfit >= 10) {
			tempProfit -= 10;
			dimes++;
		}
		while(tempProfit >= 5) {
			tempProfit -= 5;
			nickles++;
		}
		balance = 0;
		return "Your change is: " + quarters + " quarters " + dimes + " dimes " + nickles 
				+ " nickles.";
		
	}
	
	public String buyProduct(String selection) {
		balance -= inventoryMap.get(selection).getPrice();
		profit += inventoryMap.get(selection).getPrice();
		inventoryMap.get(selection).setAmountAvailable(inventoryMap.get(selection).getAmountAvailable() - 1);
		
		LocalDateTime date = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy HH:mm:ss");
		String formattedDate = date.format(formatter);
		BuyProductTransaction snackBought = new BuyProductTransaction(formattedDate + "", inventoryMap.get(selection).getName(), 
												inventoryMap.get(selection).getPrice(), balance);
		snackBought.setItemSlot(selection);
		auditReport.writer(snackBought);
		inventoryMap.get(selection).setAmountSold(inventoryMap.get(selection).getAmountSold() + 1);
		return inventoryMap.get(selection).getSound();
	}
	
	
	
}
